---
layout: handbook-page-toc
title: "TRN.1.01 - General Security Awareness Training Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TRN.1.01 - General Security Awareness Training

## Control Statement

All GitLab team-members complete security awareness training, which includes updates about relevant policies and how to report security events to the authorized response team. Records of training completion are documented and retained for tracking purposes.

## Context

At GitLab, we use internal training modules developed by the Security Operations team as GitLab's security awareness training. In the past, security awareness training has only been a task associated with on-boarding as a new GitLab team-member. As we adopt new compliance frameworks and work towards an audit certification of some of these frameworks, we will need to increase the cadence of this training. Security awareness training can be seen as a hassle and disruption from normal work, but these trainings can have real value not only to GitLab as a company, but also to GitLab team members in their personal lives. Taking the time to go through this training will ensure team members are up to date on security best practices which will help to minimize GitLab's risk as a company.

* Because this field changes so quickly, and because of our changing needs for compliance as a company, *we now require all GitLab team-members to complete this training at least every 12 months*.

## Scope

This control applies to all GitLab team-members.

## Ownership

Control ownership:
* `People Operations`

Process Owner:
* People Operations
* Security Operations

People Ops are responsible for deploying the process to ensure 100% of employee testing and validating that every GitLab team member has completed training in the current year. Security Ops is consulted for the content and effectiveness of the control. All GitLab team members are responsible for completing their security awareness training.

## Additional control information and project tracking

Currently the security training is delivered through a recorded video and team member confirms acknowledgement in a form. The training is linked from onboarding issue template as part of the new hire tasks. 

For audit evidence of compliance, we need to be able to demonstrate 100% completion of training by all team members.

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [General Security Awareness Training control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/931).


### Policy Reference
* Handbook policy on [Security Awareness Training](/handbook/security/#security-awareness-training)
* [YouTube Security Awareness Training Video](https://www.youtube.com/watch?v=kNc9f-LH6pU) and [response form](https://docs.google.com/forms/d/1_jUbA961Mj44paTfiI2VFTMsElAdjEqFsLrfPKfoW04)
* [Onboarding issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md)

## Framework Mapping


* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
  * CC2.2
  * CC2.3
* PCI
  * 12.6
  * 12.6.1
  * 12.6.2
