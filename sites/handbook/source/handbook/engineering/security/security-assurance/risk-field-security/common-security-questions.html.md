---
layout: handbook-page-toc
title: GitLab AnswerBase
description: >-
  GitLab AnswerBase is an internal only database of curated security and privacy
  relevant question and answer pairs maintained by the Risk and Field Security
  Team.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[GitLab Handbook](/handbook/)>[GitLab Handbook: Engineering](/handbook/engineering/)>[GitLab Handbook: Security Department](/handbook/engineering/security)>[GitLab Handbook: Security Assurance](/handbook/engineering/security/#assure-the-customer---the-security-assurance-sub-department)>[GitLab Handbook: Field Security](/handbook/engineering/security/security-assurance/risk-field-security/)

## GitLab AnswerBase

[GitLab AnswerBase](https://gitlab.com/gitlab-private/gl-security/security-assurance/risk-field-security-team/security-standard-answers-rfp/-/issues?scope=all&state=all&label_name[]=FY22Q2) is a set of curated security, risk, and privacy relevant question and answer pairs maintained by the Risk and Field Security Team. In the spirit of [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding) and [collaboration](https://about.gitlab.com/handbook/values/#collaboration), this tool is designed to increase speed and efficiencies when completing [Customer Security Assessments](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html) and ultimately reduce the time to complete sales activities. 

### Access and Instructions

- [GitLab AnswerBase](https://gitlab.com/gitlab-private/gl-security/security-assurance/risk-field-security-team/security-standard-answers-rfp/-/issues?scope=all&state=all&label_name[]=FY22Q2) is accessible to GitLab Team members only at this time.
- Each issue represents a unique question and answer. Some issues may have multiple questions that are phrased differently but are asking the same thing. This is done to reduce the number of issues and aid in searchability. 
- Issues are labeled and categorized in alignment with [GitLab's Control Framework](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html). For ease of searching, labels can be used to filter issues by the Control Framework section. 
- Issues are labeled to distinguish questions that are applicable to SaaS or Self-Managed and questions that might require an NDA.
- Issues may be labeled with a Compliance questionnaire reference, such as ~CAIQ or ~SIG. These questions support GitLab's [CSA's CAIQ questionnaire](https://cloudsecurityalliance.org/star/registry/gitlab/) and GitLab's Standardized Information Gathering (SIG) questionnaire.

### Maintenance and Improvements
- Quarterly, the Risk and Field Security team will conduct a quality audit of approximately 25% of the issues (for a total of 100% in a 1 year period) and update content as necessary. A quarter-specific label will be added (example: ~FY22Q1) indicating the last time it was audited. 
- The Risk and Field Security Team continuously sources new questions to add to AnswerBase with a goal of 20 new questions per quarter. Generally speaking, any question that is not readily answered within AnswerBase will be added within 14 days of being posed. Questions can be posed via the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel or by opening a new issue within the AnswerBase project. 

## Contact the Field Security Team
* Email
   * `fieldsecurity@gitlab.com`
* Slack
   * Feel free to tag is with `@field-security`
   * The `#sec-fieldsecurity`, `#sec-assurance`, `#security-department` slack channels are the best place for questions relating to our team (please add the above tag)
* [GitLab field security project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/risk-field-security/)
