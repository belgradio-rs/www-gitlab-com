---
layout: handbook-page-toc
title: "GitLab Project Management Hands On Guide- Lab 3"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab Project Management course."
---
# GitLab Project Management Hands On Guide- Lab 3
{:.no_toc}

## LAB 3- CREATE AN ISSUE AND ADD DETAILS TO IT** 

### Create an Issue

1. On the left-hand side of the screen, locate the **Issue** section on the taskbar and click on it.  
2. Click the blue **New Issue** button.  
3. In the title field type, “***Project kick off****”.  Optionally, you can enter a comment in the Description dialog box.  
4. In the Assignee field, click the link for **Assign to me**. 
5. In the Weight field, **enter 10** and assign a due date for a week from today. Leave all other fields at their default and click the **Submit Issue** button. 

### Create 3 Labels 
1. On the left-hand side of the screen, locate the **Labels** section on the taskbar and click on it.  
2. Click the blue **Create label** button.  
3. In the title field type, “***workflow::todo***”  and assign it a background color of your choosing 
4. Click the blue **Create label** button.  
Note: Your labels have been created at the project level, so they are specific to that project level. They will not be available at the group level. 
5. Repeat these steps 2 more times, creating labels for “***workflow::resolved***”  and “***workflow::backlog***”  and using label colors of your choice. 
6. You should now have 3 labels created under your Labels section.  

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Project Management- please submit your changes via Merge Request!

