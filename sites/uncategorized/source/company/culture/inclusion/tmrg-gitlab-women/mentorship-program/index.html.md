---
layout: markdown_page
title: "Women at GitLab Mentorship Program"
description: "Mentorship opportunities for women at GitLab sponspored by a partnership between the Women TMRG, GitLab Learning and Development team, and the GitLab DIB team"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Women at GitLab Mentorship Program 

The Women at GitLab Mentorship program is a career development opportunity for participants to build trust across the organization, enable pathways for internal growth and development, and increase collaboration across teams. The program is hosted in collaboration with the Women's TMRG, GitLab Learning and Development team, and the GitLab DIB team.

The Women at GitLab Mentorship Program was kicked off on 2021-07-08. In the program, we have 32 mentor/mentee pairs. Participants represent 16+ departments across GitLab. The program will run for 3 months, from 2021-07 to 2021-09. Please see the [Timeline](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#timeline) and [Metrics and Outcomes](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#metrics-and-outcomes) sections on this page for further details and program results.

### History

In 2020, the Women's TMRG partnered with the Sales Organization at GitLab to offer the [Women in Sales Mentorship Program pilot](https://about.gitlab.com/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). This initial iteration was a huge success and inspired the scale of this iteration to include a wider audeince. 

Some of the program structure for this iteration has been adapted to enable greater participation and fit the capacity of the L&D team. The mission, benefits, and goals of the program will stay the same.

### Structure

**Initial program kickoff**

All mentors and mentees will gather for a live program kickoff. This will be a panle format with a short discussion about panelists past experience with mentorship and provide an opportunity for team members to ask program questions. We'll host 3 sessions of this kickoff to accommodate workings hours of all participants. The session will run for 25 minutes.

Watch the replay of our 3 kick-off sessions below:

**Session 1:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/cuRT7nUU10Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Session 2:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/sY57W3xbLJU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Session 3:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/pR-WPq4uH5o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**1:1 Mentor Sessions**

The program proposes a mentor/mentee relationship between selected mentee applicants and mentors across the company. Sessions will take place every other week for 30-minutes and will last for up to 3 months (with the possibility of an extension), as long as both mentors and mentees remain engaged. The mentor/mentee relationship will be cross-divisional, meaning that both parties will have the opportunity to work with and learn from team members outside of their respective divisions.

**Mentor/Mentee Workbook**

[Async resources](/handbook/people-group/learning-and-development/mentor/#mentoring-resources) have been created to serve as a training guide for all mentor/mentee pairs. These resources include suggested articles to read and discuss, strategies for goal setting, sample meeting agendas, and additional training material. While it's not required that these resources guide the relationship, they are useful if mentors or mentees feel stuck or need a starting place for discussion.

**Mentor/Mentee Training**

Completion of the self-paced [How to be a Good Mentor or Mentee](https://www.linkedin.com/learning/how-to-be-a-good-mentee-and-mentor/the-power-of-mentoring?u=2255073) course on LinkedIn Learning is expected of all mentors and mentees. Depending on need and interest, we will host optional discussion sessions for mentors and mentees to discuss topics learned in the course. If you prefer to review text-based information, you can instead review [this slides](/handbook/people-group/learning-and-development/mentor/#mentor-and-mentee-training) which were created using the content from the LinkedIn course. It's important that participants make every effort to complete the course and/or review the presentation slides to ensure the greatest results from the program and relationship.

_This requirement was updated on 2021-06-28 with the following goals in mind: increase async participation, enable different learning styles, leverage high-quality content in LinkedIn Learning_

**Mid-Program Individual Growth Plan Work**

At the program half-way point, program participants will be encouraged to attend live/watch the recording of the Skill of the Month Individual Growth Plan Trainings hosted by the L&D team. The week after the training, participants will be invited to attend one of three office hour sessions to share examples, ask questions about, and discuss the IGP.

**End of Program Celebration**

To conclude the program, mentors and mentees will gather for small and full group discussions to share biggest takeaways from the program. We'll host multiple sessions of this celebration to accommodate workings hours of all participants. The celebration call will be 50 minutes long.


### Timeline

| Date | Description | Related Resources |
| ----- | ----- | ----- |
| 2021-05-05 | Discuss program details with Women's TMRG group meeting | [Planning Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/122) |
| 2021-05-20 - 2021-06-07 | Call for Mentors | Application below |
| 2021-06-07 - 2021-06-23 | Call for Mentees | Application below |
| 2021-06-29 | Pairing of mentor relationships complete and communicated to applicants | |
| Between 2021-06-29 and 2021-07-07 | Optional: Mentor/Mentee pre-program coffee chats | |
| 2021-07-07 and 2021-07-08 | [Initial program Kickoff meeting](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#structure) | |
| Week of 2021-07-12 | Self-Paced Training for Mentors and Mentees | See section on [Mentor/Mentee Training](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#structure) |
| 2021-08-11 | IGP Sessions | Training during Career Development Skill of the Month |
| 2021-08-25 | IGP Office Hours | Async + Sync participation options |
| Week of 2021-09-20 | End of program celebration | Date TBD |


### Mentoring resources

Existing [resources for mentors](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/) are available in the L&D handbook. As part of this program, a detailed mentor/mentee relationship workbook will be created in the handbook and used to guide mentoship sessions and communications. Please stay tuned for these resources as the program kick off gets closer.

### Being a Mentor

#### Benefits of being a mentor

As a mentor, you benefit from:

- Fast feedback loop between team members and leadership
- Opportunity to form relationships with team members in other departments
- Opportunity to support Women at GitLab and live our values

#### Mentor requirements

- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentee on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)
- You can complete the [DIB training certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05) before the program begins

#### Considerations

Applications will be prioritized on many points including:

- Skill and prior experience as a mentor
- Performance in current role
- Available and appropriate mentee match in the current session

#### Apply to be a Mentor

Please complete [this Mentor Application Google form](https://docs.google.com/forms/d/e/1FAIpQLSfkoP5XXB4Zz_wVqbEvrMlATmp710A9CDRM6GegXRIxSX4KqQ/viewform) to apply to be a mentor.

Please note that applying for a mentor does not guarantee you a spot in the program, and eligibility will be evaluated based on criteria above.

If you've participated in a previous GitLab mentor program, you are welcome to apply again!

### Being a Mentee

#### Benefits of being a mentee

As a mentee, you benefit from:

- Increased visibility with leadership
- Increased professional development opportunities
- Career coaching and guidance
- Opportunity to form relationships with leaders on other teams

#### Mentee requirements

- You're a woman-identifying team member at GitLab. Gender non-conforming folks who face workplace challenges similar to those of women-identifying team members are also welcome to apply.
- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentor on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)

#### Considerations

Applications will be prioritized on many points including:

- Team members on a management career track
- Performance in current role

#### Apply to be a Mentee

Application for mentees will open on 2021-06-07.
Please complete [this Mentee Application Google form](https://docs.google.com/forms/d/e/1FAIpQLSfzKUipyNHZKC90ZHcAbOkhAi4a_pqMhmCOkAodLeDBgxII_w/viewform) to apply to be a mentee.

Please note that applying for a mentee does not guarentee you a spot in the program, and eligibility will be evaluated based on criteria above.


### Metrics and Outcomes

Mentors and mentees will complete a pre-program and end of program survey to collect feedback, assess skills, and determine if goals set during mentor/mentee relationships have been set.

Success of this program will be measured by:

1. percentage of mentor/mentee pairs who meet consistently on a bi-weekly basis, with a goal of 95% completion
1. attendance to live training sessions, with a goal of 95% attendance
1. engagement during live training sessions
1. overall feedback of the program from both the mentor and mentee perspective

### Results

The following section will be updated throughout the program to reflect participations and metrics outlined above.

#### Program Participation

##### Participant Data

1. Total Participants: 66 team members / 33 mentor/mentee pairs
1. Unique Departments Represented by Mentees: 19
1. Unique Departments Represented by Mentors: 18

##### Event Attendance

| Event Name | Total Attendance Percentage | Attendance Session 1/Session 2/Session 3 |
| ----- | ----- | ----- |
| Program Kickoff | 95.31% | 7/12/42 |
| Mid-Program Event | 4.5% | 0 / 0 / 3 |
| End of Program Event | TBD | TBD |

##### 1:1 Mentorship Meeting Data

Based on a survey sent via the Polly Slack app on 2021-07-23:

1. 20/33 mentees responded to the survey
1. 4 have met and/or scheduled their coffee chat with their mentor
1. 13 have met for their first mentorship session and set a cadence for future meetings
1. 4 have their first mentorship session scheduled for next week (week of 2021-07-26)
1. 1 falls in an `other` category

##### Pre-Program Survey

| Participant | Completions |
| ----- | ----- |
| Mentor | 20/33 |
| Mentee | 16/33 |

###### Mentor Results

![mentor-data](/sites/uncategorized/source/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/mentor-data.jpg)

*What is something you hope to learn from your mentee during this program?*

1. Be an ally - understand and support challenges for women at GitLab and team members in different departments
1. New leadership perspectives
1. What kind of support will provide the most help
1. How well GitLab is supporting professional development of the team
1. Communication skills
1. Self-Reflection skills

*What skills do you hope this program will help you develop and/or strengthen?*

1. Situational coaching
1. Mentorship
1. Listening
1. Recieving Feedback
1. Leadership
1. Awareness
1. Coaching
1. Professional Development
1. Empathy
1. Cultural awareness
1. Motivation/Empowerment
1. Work/Life Balance
1. Cross-Functional Collaboration
1. Communication
1. Self-Reflection
1. Confidence
1. Planning

##### Mentee Results

![mentee-data](/sites/uncategorized/source/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/mentee-data.jpg)

**What is one goal you hope to accomplish by participating in this Women at GitLab mentorship program?**

1. Leadership growth, executive leadership skills, and confidence in working with leadership
1. Develop career path
1. Improve cross-functional relationships
1. Negotiation skills
1. Build confidence in career path and goals
1. New inspiration

**What skills do you hope this program will help you develop and/or strengthen?**

1. Leadership
1. Communication
1. Introspection
1. Confidence
1. Self-promotion
1. Self-value
1. Negotiation
1. Strategy and idea pitching
1. People Management
1. Public speaking
1. Collaborative working style
1. Building team morale
1. Empowerment
1. Effective written and verbal communication
1. Goal Setting
1. Strategic thinking


##### Post-Program Survey:

TBD, Shared during the week of 2021-09-20


### Common Questions

**Can I participate as both a mentor and a mentee?**

Yes, this is a possibility. It's important that team members talk with their managers to discuss their capacity to participate in both roles in this program. Participation in both roles will require a 2hr commitment per month (two 30 minute sessions as a mentor and two 30 minute sessions as a mentee).

It's possible that we will have a shortage of mentors and be unable to complete all mentor/mentee pairs. If there is a shortage of mentors and you've applied as both a mentor and a mentee, you will be assigned **only** as a mentor in this program. 

**I'd like to participate but I can't commit the entire length of the program. Can I still apply?**

If you cannot commit to the 3 month program, please consider applying for a future mentorship program at GitLab. It's important that both mentors and mentees are available for the full 3 month program to achieve the best results.

**I'm not eligible to be a mentor or a mentee in this program. What are my options for future participation?**

The L&D team is working with the DIB team to create a mentor program framework that can be implemented for teams across GitLab. Please watch for updates on this iteration in FY22-Q3.

In addition, team members are always encouraged to build mentor/mentee relationships independently. You can find resources and a list of available mentors in the [Learning and Development handbook](/handbook/people-group/learning-and-development/mentor/)


### Additional questions?

Questions, comments, and feedback can be shared in this [issue](https://gitlab.com/gitlab-com/women-tmrg/-/issues/31). For additional questions, please reach out to `@slee24` in the [#women](https://app.slack.com/client/T02592416/C7V402V8X) or [#learninganddevelopment](https://app.slack.com/client/T02592416/CMRAWQ97W) Slack channels with questions or concerns.
